'''
Unit tests for diplomacy_solve()
corner cases, failure cases
3 tests min
'''
from Diplomacy import diplomacy_solve
from unittest import main, TestCase
from io import StringIO

class TestDiplomacy(TestCase):
    # ----
    # diplomacy_solve
    # ----
    def test_solve_1(self):
        r = StringIO('A Madrid Hold\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\n')

    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')

    def test_solve_4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_solve_5(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n D Paris Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC [dead]\nD Paris\n')

    def test_solve_6(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

    def test_solve_7(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support B\nE Austin Move Paris\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB [dead]\nC London\nD [dead]\nE [dead]\n')
    
    def test_solve_8(self):
        r = StringIO('B Barcelona Move Madrid\nC London Support B\nA Madrid Hold\nD Austin Move London\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
    
    def test_solve_9(self):
        r = StringIO()
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), '')

if __name__ == "__main__":
    main()